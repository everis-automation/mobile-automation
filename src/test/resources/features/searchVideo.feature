#language:es
Característica: Buscar un video
  Yo como usuario de Youtube
  Quiero buscar un video en la app
  Para encontrar mi video favorito

  @SEARCH_VIDEO
  Esquema del escenario: Buscar los siguientes videos y validar el resultado
    Dado que me encuentro en la aplicación YouTube
    Cuando busco el video "<video>"
    Entonces debería obtener más de un resultado

    Ejemplos:
      | video               |
      | Appium Tutorial     |
      | Selenium Tutorial   |
      | Cypress vs Selenium |