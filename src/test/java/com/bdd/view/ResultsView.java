package com.bdd.view;

import com.mobile.MobileBase;
import com.mobile.MobileDriverManager;
import com.mobile.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ResultsView extends MobileBase {

    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.youtube:id/results']/android.view.ViewGroup")
    private List<MobileElement> resulList;

    public int getSizeResult() {
        WebDriverWait webDriverWait = new WebDriverWait(MobileDriverManager.getDriver(), 15);
        webDriverWait.until(ExpectedConditions.visibilityOf(resulList.get(0)));
        Util.takeScreenShoot();
        return resulList.size();
    }

}
