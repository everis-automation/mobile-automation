package com.bdd.view;

import com.mobile.MobileBase;
import com.mobile.MobileDriverManager;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomeView extends MobileBase {

    @AndroidFindBy(accessibility = "Search")
    private MobileElement searchButton;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Search YouTube']")
    private MobileElement searchInput;

    public void goSearch() {
        searchButton.click();
    }

    public void typeVideo(String video) {
        searchInput.sendKeys(video);
    }

    public void enterKeyBoard() {
        ((AndroidDriver) MobileDriverManager.getDriver()).pressKey(new KeyEvent(AndroidKey.ENTER));
    }

}
