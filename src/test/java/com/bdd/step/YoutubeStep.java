package com.bdd.step;

import com.bdd.view.HomeView;
import com.bdd.view.ResultsView;

public class YoutubeStep {

    private HomeView homeView(){
        return new HomeView();
    }

    private ResultsView resultsView(){
        return new ResultsView();
    }

    public void searchVideo(String video){
        homeView().goSearch();
        homeView().typeVideo(video);
        homeView().enterKeyBoard();
    }

    public int getSizeResult(){
        return resultsView().getSizeResult();
    }

}
