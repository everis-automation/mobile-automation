package com.bdd.glue;

import com.bdd.step.YoutubeStep;
import com.mobile.ManageScenario;
import com.mobile.MobileDriverManager;
import com.mobile.Util;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.junit.jupiter.api.Assertions;

public class YoutubeGlue {

    private YoutubeStep youtubeStep;
    private Scenario scenario;

    @Before
    public void beforeScenario(Scenario scenario) {
        this.scenario = scenario;
        ManageScenario.setScenario(this.scenario);
        youtubeStep = new YoutubeStep();
    }

    @After
    public void afterScenario() {
        if(this.scenario.isFailed())
            Util.takeScreenShoot();

        MobileDriverManager.quitDriver();
    }

    @Dado("que me encuentro en la aplicación YouTube")
    public void queMeEncuentroEnLaAplicacionYoutube() {
        MobileDriverManager.setMobileDriver();
    }

    @Cuando("busco el video {string}")
    public void buscoElVideo(String video) {
        youtubeStep.searchVideo(video);
        Util.takeScreenShoot();
    }

    @Entonces("debería obtener más de un resultado")
    public void deberíaObtenerMásDeUnResultado() {

        Assertions.assertTrue(youtubeStep.getSizeResult() > 0, "El tamaño de la lista es igual a 0.");
    }
}
